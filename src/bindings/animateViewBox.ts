import * as ko from "knockout";
import * as utils from "../utils/utils";
import { SvgViewBox } from "../view/svg";

export function applyBinding() {

  ko.bindingHandlers.animateViewBox = {
    init: function(element, valueAccessor, allBindings) {
      // This will be called when the binding is first applied to an element
      // Set up any initial state, event handlers, etc. here
      let value = ko.unwrap(valueAccessor());
      let prevValue = value;
      element.setAttribute('viewBox', `${value.x} ${value.y} ${value.width} ${value.height}`);

      function update(value: SvgViewBox) {
        let delta = utils.getDelta(prevValue, value);
        let tmp = prevValue;

        const max = 15;
                
        const sx = delta.x/max;
        const sy = delta.y/max;
        const sw = delta.width/max;
        const sh = delta.height/max;

        let animate = () => {
            const delta = utils.getDelta(tmp, value);

            tmp.x = (Math.abs(delta.x) <= Math.abs(sx)) ? value.x : (tmp.x - sx);
            tmp.y = (Math.abs(delta.y) <= Math.abs(sy)) ? value.y : (tmp.y - sy);
            tmp.width = (Math.abs(delta.width) <= Math.abs(sw)) ? value.width : (tmp.width - sw);
            tmp.height = (Math.abs(delta.height) <= Math.abs(sh)) ? value.height : (tmp.height - sh);

            element.setAttribute('viewBox', `${utils.round(tmp.x)} ${utils.round(tmp.y)} ${utils.round(tmp.width)} ${utils.round(tmp.height)}`)

            if (!utils.compareObjects(value, tmp)) {
              requestAnimationFrame(animate)
            } else {
              prevValue = value;
            }
        }

        animate();
      }

      ko.computed(function () {
            let value = ko.unwrap(valueAccessor());
            update(value);
      }, null, { disposeWhenNodeIsRemoved: element });
    }
  };
}
