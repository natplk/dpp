import * as ko from "knockout";
import * as CONST from "../utils/const";
import { IDrawable } from "./drawable";
import { Box } from "../view/box";
import { Offset } from "../view/offset";
import { IShape } from "../view/shape";
import { Caption } from "../view/caption";
import { Size } from "../view/size";
import { ViewConfig } from "../photoPitchView";

export enum ObstacleType {
    Camera,
    PowerSupply,
    BigCamera,
    BigPowerSupply
}

export enum ObstaclePlacement {
    After,
    Before
}

export class Obstacle implements IDrawable {
    seatNumber: number;
    type: ObstacleType;
    placement: ObstaclePlacement;
    size: Size;
    shape: IShape;
    caption: Caption;
    offsetStart: Offset = {x: 0, y: 0};
    boundingBox: Box;
    
    constructor(type: ObstacleType, seatNumber: number, placement: ObstaclePlacement) {
        this.type = type;
        this.seatNumber = seatNumber;
        this.placement = placement;
    }

    draw(x: number, y: number, viewConfig: ViewConfig) {
        let unit = viewConfig.unit;
        let space = CONST.unitsInSpaceBeetweenSeats * unit;

        let shapeX = x + this.offsetStart.x + space;
        let shapeY = y + this.offsetStart.y + space;
        let shapeW = this.size.width;
        let shapeH = this.size.height;

        this.shape = new Box(shapeX, shapeY, shapeW, shapeH);

        return {
            x: x + this.size.width + space,
            y: y + this.size.height + space,
        }
    }
}
