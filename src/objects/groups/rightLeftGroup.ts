﻿
import * as CONST from "../../utils/const";
import { Group } from "../group";
import { Seat } from "../seat";
import { ObstaclePlacement, Obstacle } from "../obstacle";
import { ViewConfig } from "../../photoPitchView";
import * as View from "../../view";

export class RightLeftGroup extends Group {

    constructor(label: string) {
        super(label);
    }

    draw(x: number, y: number, viewConfig: ViewConfig) {
        const unit = viewConfig.unit;
        const offsetStart = this.offsetStart;
        const space = 1.5 * CONST.unitsInSpaceBeetweenSeats * unit ;
        
        let offset = {x: x, y: y};
        let tmp: View.Offset;

        let drawObstacle = (obstacle: Obstacle, seat: Seat) => {
            obstacle.offsetStart.x = - obstacle.size.width - space;
            obstacle.offsetStart.y = offsetStart.y + seat.size.height - (obstacle.size.height + seat.size.height) / 2;
            tmp = obstacle.draw(offset.x, y, viewConfig);
            offset.x -= tmp.x - offset.x;
        }

        this.seats.map((seat: Seat) => {
            let obstaclesAfter = this.obstacles.filter(obstacle => (obstacle.seatNumber === seat.number) && (obstacle.placement === ObstaclePlacement.After));
            let obstaclesBefore = this.obstacles.filter(obstacle => (obstacle.seatNumber === seat.number) && (obstacle.placement === ObstaclePlacement.Before));

            obstaclesBefore.map(obstacle => drawObstacle(obstacle, seat));

            seat.offsetStart.x = - seat.size.width - space;
            seat.offsetStart.y = offsetStart.y;
            tmp = seat.draw(offset.x, y, viewConfig);
            offset.x -= tmp.x - offset.x;

            obstaclesAfter.map(obstacle => drawObstacle(obstacle, seat));
        });

        let boxX = offset.x;
        let boxY = offset.y;
        let boxW = x - offset.x;
        let boxH = this.size.height;

        let shapeX = boxX + this.offsetStart.x;
        let shapeY = boxY + this.offsetStart.y;
        let shapeW = boxW;
        let shapeH = unit;

        this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);

        let captionSize = boxH * CONST.captionGroupSizeRatio;
        let captionShift = CONST.captionGroupShiftRatio * captionSize;
        let captionX = boxX + boxW / 2;
        let captionY = boxY + captionShift;

        this.caption = new View.Caption(this.label, captionX, captionY, captionSize);

        this.boundingBox = new View.Box(boxX, boxY, boxW, boxH);
        this.boundingBox.setBorders(CONST.borderRatio * unit, "vertical");

        return offset;
    }
}

