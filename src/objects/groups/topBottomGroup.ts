﻿import * as CONST from "../../utils/const";
import { Group } from "../group";
import { Seat } from "../seat";
import { ObstaclePlacement, Obstacle } from "../obstacle";
import { ViewConfig } from "../../photoPitchView";
import * as View from "../../view";

export class TopBottomGroup extends Group {

    constructor(label: string) {
        super(label);
    }

    draw(x: number, y: number, viewConfig: ViewConfig) {
        const width = viewConfig.width;
        const unit = viewConfig.unit;
        const space =CONST.unitsInSpaceBeetweenSeats * unit / 2;

        let offset = {x: x, y: y};
        let offsetStart = this.offsetStart;
        let tmp: View.Offset;

        let drawObstacle = (obstacle: Obstacle, seat: Seat) => {
            obstacle.offsetStart.x = offsetStart.x + seat.size.width - (obstacle.size.width + seat.size.width) / 2;
            obstacle.offsetStart.y = -space;
            tmp = obstacle.draw(x, offset.y, viewConfig);
            offset.y += tmp.y - offset.y;
        }

        this.seats.map((seat: Seat) => {
            let obstaclesAfter = this.obstacles.filter(o => (o.seatNumber === seat.number) && (o.placement === ObstaclePlacement.After));
            let obstaclesBefore = this.obstacles.filter(o => (o.seatNumber === seat.number) && (o.placement === ObstaclePlacement.Before));

            obstaclesBefore.map(obstacle => drawObstacle(obstacle, seat));

            seat.offsetStart.x = offsetStart.x;
            seat.offsetStart.y = -space;
            tmp = seat.draw(x, offset.y, viewConfig);
            offset.y += tmp.y - offset.y;

            obstaclesAfter.map(obstacle => drawObstacle(obstacle, seat));
        });

        let boxX = x;
        let boxY = y;
        let boxW = this.size.width;
        let boxH = offset.y - y;

        let shapeX = boxX + this.offsetStart.x;
        let shapeY = boxY + this.offsetStart.y;
        let shapeW = unit;
        let shapeH = boxH;

        this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);

        let isRightHand = x > width / 2;

        let captionSize = boxW * CONST.captionGroupSizeRatio;
        let captionShift = CONST.captionGroupShiftRatio * captionSize;
        let captionX = boxX + (isRightHand ? boxW - captionShift : captionShift);
        let captionY = boxY + boxH / 2 + captionSize / 3;

        this.caption = new View.Caption(this.label, captionX, captionY, captionSize);

        this.boundingBox = new View.Box(boxX, boxY, boxW, boxH);
        this.boundingBox.setBorders(CONST.borderRatio * unit, "horizontal");

        return offset;
    }
}

