﻿import * as CONST from "../../utils/const";
import { Group } from "../group";
import { Seat } from "../seat";
import { ObstaclePlacement, Obstacle } from "../obstacle";
import { ViewConfig } from "../../photoPitchView";
import * as View from "../../view";

export class BottomTopGroup extends Group {

    constructor(label: string) {
        super(label);
    }

    draw(x: number, y: number, viewConfig: ViewConfig) {
        const width = viewConfig.width;
        const unit = viewConfig.unit;
        const space = 1.5 * CONST.unitsInSpaceBeetweenSeats * unit;

        let offset = {x: x, y: y};
        let offsetStart = this.offsetStart;
        let tmp: View.Offset;


        let drawObstacle = (obstacle: Obstacle, seat: Seat) => {
            obstacle.offsetStart.x = offsetStart.x + seat.size.width - (obstacle.size.width + seat.size.width) / 2;
            obstacle.offsetStart.y = - obstacle.size.height - space;
            tmp = obstacle.draw(offset.x, offset.y, viewConfig);
            offset.y -= tmp.y - offset.y;
        }

        this.seats.map((seat: Seat) => {
            let obstaclesAfter = this.obstacles.filter(obstacle => (obstacle.seatNumber === seat.number) && (obstacle.placement === ObstaclePlacement.After));
            let obstaclesBefore = this.obstacles.filter(obstacle => (obstacle.seatNumber === seat.number) && (obstacle.placement === ObstaclePlacement.Before));
            obstaclesBefore.map(obstacle => drawObstacle(obstacle, seat));

            seat.offsetStart.x = offsetStart.x;
            seat.offsetStart.y = - seat.size.height - space;
            tmp = seat.draw(offset.x, offset.y, viewConfig);
            offset.y -= tmp.y - offset.y;

            obstaclesAfter.map(obstacle => drawObstacle(obstacle, seat));
        });


        let boxX = x;
        let boxY = offset.y;
        let boxW = this.size.width;
        let boxH = y - offset.y;

        let shapeX = boxX + this.offsetStart.x;
        let shapeY = boxY + this.offsetStart.y;
        let shapeW = unit;
        let shapeH = boxH;

        this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);

        let isRightHand = x > width / 2;

        let captionSize = boxW * CONST.captionGroupSizeRatio;
        let captionShift = CONST.captionGroupShiftRatio * captionSize;
        let captionX = boxX + (isRightHand ? boxW - captionShift : captionShift);
        let captionY = boxY + boxH / 2 + captionSize / 3;

        this.caption = new View.Caption(this.label, captionX, captionY, captionSize);

        this.boundingBox = new View.Box(boxX, boxY, boxW, boxH);
        this.boundingBox.setBorders(CONST.borderRatio * unit, "horizontal");

        return offset;
    }
}
