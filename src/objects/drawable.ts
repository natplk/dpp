import { ViewConfig } from "../photoPitchView"

import * as View from "../view";

export interface IDrawable {
    shape: View.IShape;
    caption: View.Caption;
    offsetStart: View.Offset;
    boundingBox: View.Box;
    
    draw(x: number, y: number, viewConfig: ViewConfig): View.Offset
}