import * as ko from "knockout";
import * as CONST from "../utils/const";
import { IDrawable } from "./drawable";

import { ViewConfig } from "../photoPitchView"

import * as View from "../view";

export enum SeatState {
        Available,
        Taken,
        TakenByTheSameOrganisation,
        AvailableSelected,
    }

    export class Seat implements IDrawable {
        number: number;
        state: KnockoutObservable<SeatState>;
        label: string;
        isVisible: KnockoutObservable<boolean>;
        size: View.Size;
        shape: View.IShape;
        caption: View.Caption;
        offsetStart: View.Offset = { x: 0, y: 0 };
        boundingBox: View.Box;

        constructor() {
            this.state = ko.observable(SeatState.Available);
            this.isVisible = ko.observable(true);
        }

        draw(x: number, y: number, viewConfig: ViewConfig) {
            const unit = viewConfig.unit;
            const space = CONST.unitsInSpaceBeetweenSeats * unit;

            let shapeX = x + this.offsetStart.x + space;
            let shapeY = y + this.offsetStart.y + space;
            let shapeW = this.size.width;
            let shapeH = this.size.height;

            this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);

            let captionSize = shapeW / 2.5;
            let captionX = shapeX + shapeW / 2;
            let captionY = shapeY + shapeH / 2 + shapeW / 6;

            this.caption = new View.Caption(this.number.toString(), captionX, captionY, captionSize);
            this.boundingBox = new View.Box(shapeX, shapeY, shapeW, shapeH);

            return {
                x: x + this.size.width + space,
                y: y + this.size.height + space
            }
        }
    }