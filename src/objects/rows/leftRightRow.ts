﻿import * as CONST from "../../utils/const";
import { Row } from "../row";
import { LeftRightGroup } from "../groups/leftRightGroup";
import { ViewConfig } from "../../photoPitchView";
import * as View from "../../view";

export class LeftRightRow extends Row {
    
    constructor(label: string) {
        super(label);
    }

    draw(x: number, y: number, viewConfig: ViewConfig) {
        const {height, unit} = viewConfig;
        const isRightHand = x > height / 2;

        let offset = {x: x, y: y};

        this.groups.map((group: LeftRightGroup) => {
            group.size.height = this.size.height;
            group.offsetStart = this.offsetStart;
            offset = group.draw(offset.x, offset.y, viewConfig);
        });

        let boxX = x;
        let boxY = y;
        let boxW = this.size.width;
        let boxH = this.size.height;

        let shapeX = x + this.offsetStart.x;
        let shapeY = y + this.offsetStart.y;
        let shapeW = offset.x - x;
        let shapeH = unit;

        this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);

        let captionSize = height * CONST.captionRowSizeRatio;
        let captionShift = CONST.captionRowShiftRatio * captionSize;
        let captionX = boxX + boxW / 2;
        let captionY = boxY + (isRightHand ? boxH - captionShift : captionShift);

        this.caption = new View.Caption(this.label, captionX, captionY, captionSize);
        this.caption.rotate(isRightHand ? 180 : 0);

        this.boundingBox = new View.Box(boxX, boxY, boxW, boxH);
        this.boundingBox.setBorders(CONST.borderRatio * unit, "vertical");

        return offset;
    }
}

