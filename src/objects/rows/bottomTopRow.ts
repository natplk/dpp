﻿import * as CONST from "../../utils/const";
import { Row } from "../row";
import { BottomTopGroup } from "../groups/bottomTopGroup";
import { ViewConfig } from "../../photoPitchView";
import * as View from "../../view";

    export class BottomTopRow extends Row {

        constructor(label: string) {
            super(label);
        }

        draw(x: number, y: number, viewConfig: ViewConfig) {
            const {width, height, unit} = viewConfig;
            const isRightHand = x > width / 2;

            let offset = {x: x, y: y};

            this.groups.map((group: BottomTopGroup) => {
                group.size.width = this.size.width;
                group.offsetStart = this.offsetStart;
                offset = group.draw(offset.x, offset.y, viewConfig);
            });

            let boxX = offset.x;
            let boxY = y - this.size.height;
            let boxW = this.size.width;
            let boxH = this.size.height;

            let shapeX = boxX + this.offsetStart.x;
            let shapeY = offset.y + this.offsetStart.y;
            let shapeW = unit;
            let shapeH = y - offset.y;

            this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);

            let captionSize = height * CONST.captionRowSizeRatio;
            let captionShift = CONST.captionRowShiftRatio * captionSize;
            let captionX = boxX + (isRightHand ? boxW - captionShift : captionShift);
            let captionY = boxY + boxH / 2;

            this.caption = new View.Caption(this.label, captionX, captionY, captionSize);
            this.caption.rotate(isRightHand ? 90 : -90);

            this.boundingBox = new View.Box(boxX, boxY, boxW, boxH);
            this.boundingBox.setBorders(CONST.borderRatio * unit, "horizontal");

            return offset;
        }
    }
