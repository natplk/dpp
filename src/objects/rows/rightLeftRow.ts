﻿
import * as CONST from "../../utils/const";
import { Row } from "../row";
import { RightLeftGroup } from "../groups/rightLeftGroup";
import { ViewConfig } from "../../photoPitchView";
import * as View from "../../view";

export class RightLeftRow extends Row {
    
    constructor(label: string) {
        super(label);
    }

    draw(x: number, y: number, viewConfig: ViewConfig) {
        const {height, unit} = viewConfig;
        
        let offset = {x: x, y: y};

        this.groups.map((group: RightLeftGroup) => {
            group.size.height = this.size.height;
            group.offsetStart = this.offsetStart;
            offset = group.draw(offset.x, offset.y, viewConfig);
        });

        let boxX = x - this.size.width;
        let boxY = y;
        let boxW = this.size.width;
        let boxH = this.size.height;

        let shapeX = offset.x + this.offsetStart.x;
        let shapeY = y + this.offsetStart.y;
        let shapeW = x - offset.x;
        let shapeH = unit;

        this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);

        let captionSize = height * CONST.captionRowSizeRatio;
        let captionShift = CONST.captionRowShiftRatio * captionSize;
        let captionX = boxX + boxW / 2;
        let captionY = boxY + captionShift;

        this.caption = new View.Caption(this.label, captionX, captionY, captionSize);

        this.boundingBox = new View.Box(boxX, boxY, boxW, boxH);
        this.boundingBox.setBorders(CONST.borderRatio * unit, "vertical");

        return offset;
    }
}
