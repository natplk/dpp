export class Control {
    label: string;
    value: KnockoutObservable<number>;
}
