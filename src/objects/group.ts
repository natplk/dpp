import * as ko from "knockout";
import { Seat, SeatState } from "./seat";
import { Obstacle } from "./obstacle";
import { Order, INumerable } from "./numerable";
import { IDrawable } from "./drawable";

import { ViewConfig } from "../photoPitchView"

import * as View from "../view";

export class Group implements INumerable, IDrawable {
        seats: Seat[];
        obstacles: Obstacle[];
        label: string;
        order: Order;
        isVisible: KnockoutObservable<boolean>;
        size: View.Size;
        shape: View.IShape;
        caption: View.Caption;
        offsetStart: View.Offset;
        boundingBox: View.Box;

        constructor(label: string) {
            this.label = label;
            this.seats = [];
            this.obstacles = [];
            this.isVisible = ko.observable(true);
            this.offsetStart = { x: 0, y: 0 };
            this.size = { width: 0, height: 0 };
        }

        setSeats(seats: Seat[]) {
            this.seats = seats;
        }

        setObstacles(obstacles: Obstacle[]) {
            this.obstacles = obstacles;
        }

        getSeatsAmount() {
            return this.seats.length;
        }

        getObstaclesAmount() {
            return this.obstacles.length;
        }

        hasSelectedSeat() {
           return !!this.seats.filter(seat => seat.state() === SeatState.AvailableSelected).length
        }

        setNumbers(numberStart: number) {
            let number = numberStart;

            this.seats.map(seat => {
                seat.number = number;

                if (this.order === Order.Asceding) {
                    number += 1;
                } else {
                    number -= 1;
                }
            });
        }

        draw(x: number, y: number, viewConfig: ViewConfig) {
            return { x: x, y: y }
        }
    }
