import * as ko from "knockout";
import * as CONST from "../utils/const";
import { IDrawable } from "./drawable";

import { ViewConfig } from "../photoPitchView"

import * as View from "../view";

export enum IndicatorPlacement {
        Top,
        Left, 
        Right
    }

    export class Indicator implements IDrawable {
        placement: IndicatorPlacement;
        isVisible: KnockoutObservable<boolean>;
        size: View.Size;
        shape: View.IShape;
        caption: View.Caption;
        offsetStart: View.Offset = { x: 0, y: 0 };
        boundingBox: View.Box;
        referenceBox: View.Box;

        constructor(referenceBox: View.Box) {
            this.referenceBox = referenceBox;
            this.isVisible = ko.observable(true);
            this.placement = IndicatorPlacement.Top;
        }

        draw(x: number, y: number) {
            var translateX = 0;
            var translateY = 0;
            var rotate = 0;
            var points = `${x},${y} ${x + this.size.width},${y} ${x + this.size.width / 2}, ${y + this.size.height * 0.85}`;

            switch (this.placement) {
                case IndicatorPlacement.Left:
                    translateX = -this.size.width;
                    translateY = -this.size.height;
                    rotate = -90;
                    break;
                case IndicatorPlacement.Right:
                    translateY = -2 * this.size.height;
                    rotate = 90;
                    break;
                default:
                    translateY = -this.size.height;
            }

            this.shape = new View.Polygon(points);
            this.shape.rotate(rotate, x, y);
            this.shape.translate(translateX, translateY);

            return {
                x: x + this.size.width, 
                y: y + this.size.height
            }
        }
    }