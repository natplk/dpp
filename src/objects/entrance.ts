import * as ko from "knockout";
import * as CONST from "../utils/const";
import { IDrawable } from "./drawable";

import { ViewConfig } from "../photoPitchView"

import * as View from "../view";

export enum EntrancePlacement {
    LowerRight,
    UpperRight,
    LowerLeft,
    UpperLeft,
    Center,
}

export class Entrance implements IDrawable {
        placement: EntrancePlacement;
        isVisible: KnockoutObservable<boolean>;
        size: View.Size;
        shape: View.IShape;
        caption: View.Caption;
        offsetStart: View.Offset;
        boundingBox: View.Box;

        constructor(placement: EntrancePlacement) {
            let tmp = placement;
            this.placement = (typeof tmp === "string") ? parseInt(tmp) : placement;
            this.isVisible = ko.observable(true);
        }

        draw(x: number, y: number, viewConfig: ViewConfig) {
            let width = viewConfig.width;
            let height = viewConfig.height;
            let margin = viewConfig.margin;
            let rotate = 0;
            this.size = new View.Size(margin.y / 3);

            switch (this.placement) {
                case EntrancePlacement.Center:
                    x = width / 2 - this.size.width / 2;
                    y = this.size.height / 4;
                    rotate = 180;
                break;
                case EntrancePlacement.LowerRight:
                    x = width - margin.x + this.size.width;
                    y = height - margin.y + this.size.height;
                    rotate = -45;
                break;
                case EntrancePlacement.UpperRight:
                    x = width - margin.x + this.size.width;
                    y = margin.y - 2 * this.size.height;
                    rotate = -145;
                break;
                case EntrancePlacement.UpperLeft:
                    x = margin.x - 2 * this.size.width;
                    y = height - margin.y + this.size.height;
                    rotate = 45;
                break;
                case EntrancePlacement.LowerLeft:
                    x = margin.x - 2 * this.size.width;
                    y = margin.y - 2 * this.size.height;
                    rotate = -135;
                break;
            }

            let shapeX = x;
            let shapeY = y;
            let shapeW = this.size.width;
            let shapeH = this.size.height;

            this.shape = new View.Box(shapeX, shapeY, shapeW, shapeH);
            this.shape.rotate(rotate);

            return { 
                x: x + this.size.width, 
                y: y + this.size.height
            }
        }
    }