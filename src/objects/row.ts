import * as ko from "knockout";
import { Group } from "./group";
import { Order, INumerable } from "./numerable";
import { IDrawable } from "./drawable";

import { ViewConfig } from "../photoPitchView"

import * as View from "../view";

export class Row implements INumerable, IDrawable {
        groups: Group[];
        label: string;
        order: Order;
        hasOneGroup: boolean;
        isVisible: KnockoutObservable<boolean>;
        size: View.Size;
        shape: View.IShape;
        caption: View.Caption;
        offsetStart: View.Offset = { x: 0, y: 0 };
        boundingBox: View.Box;

        constructor(label: string) {
            this.label = label;
            this.groups = [];
            this.isVisible = ko.observable(true);
            this.offsetStart = { x: 0, y: 0 };
            this.size = { width: 0, height: 0 };
        }

        setGroups(groups: Group[]) {
            this.groups = groups;
            this.hasOneGroup = this.getGroupsAmount() === 1;
        }

        getGroupsAmount() {
            return this.groups.length;
        }

        getObstaclesAmount() {
            return this.groups.map((group: Group) => group.getObstaclesAmount())
                .reduce((prev: number, cur: number) => prev + cur, 0);
        }

        getSeatsAmount() {
            return this.groups.map((group: Group) => group.getSeatsAmount())
                .reduce((prev: number, cur: number) => prev + cur, 0);
        }

        setNumbers(numberStart: number) {
            let groupNumberStart = numberStart;

            this.groups.map(group => {
                group.order = this.order;
                group.setNumbers(groupNumberStart);

                let seatsAmount = group.getSeatsAmount();

                if (this.order === Order.Asceding) {
                    groupNumberStart += seatsAmount
                } else {
                    groupNumberStart -= seatsAmount;
                }
            })
        }

        draw(x: number, y: number, viewConfig: ViewConfig) {
            return {x: x, y: y}
        }
    }
