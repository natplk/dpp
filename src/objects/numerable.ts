export enum Order {
    Asceding,
    Descending
}

export interface INumerable {
    order: Order,
    setNumbers(numberStart: number): void
}