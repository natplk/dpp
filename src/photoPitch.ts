import "../styles/photo-pitch.css";
import { applyBinding } from "./bindings/animateViewBox";
import * as ko from "knockout";
import * as utils from "./utils/utils";
import * as CONST from "./utils/const";
import * as Objects from "./objects";

import { PhotoPitchBuilder } from "./photoPitchBuilder";
import { PhotoPitchView}  from "./photoPitchView";

applyBinding();

class UnavailableSeatsViewModel {
    unavailableSeats: KnockoutComputed<number[]>;
    unavailableSeatsIndex: KnockoutObservable<string>;

    constructor() {
        this.unavailableSeatsIndex = ko.observable('140, 50-70');
        this.unavailableSeats = ko.computed(this.parseSeatsIndexes);
    }

    public parseSeatsIndexes = () => {
        const colon = /\s*,\s*/;
        const dash = /\s*-\s*/;
        const indexes = this.unavailableSeatsIndex().split(colon);
        let unavailableSeats: number[] = [];

        indexes.map((r: string) => {
            const subRange = r.split(dash);

            if (subRange.length === 1) {
                unavailableSeats.push(parseFloat(r));
            }

            if (subRange.length === 2) {
                for (let i: any = parseFloat(subRange[0]); i <= subRange[1]; i++ ) {
                    unavailableSeats.push(i);
                }
            }
        });

        return unavailableSeats;
    }
};

class ObstacleViewModel {
    obstacles: KnockoutObservableArray<Objects.Obstacle>;
    obstacleType: KnockoutObservable<Objects.ObstacleType>;
    obstacleIndex: KnockoutObservable<string>;
    obstaclePlacement: KnockoutObservable<Objects.ObstaclePlacement>;

    constructor() {
        this.obstacles = ko.observableArray([]);
        this.obstacleIndex = ko.observable();
        this.obstacleType = ko.observable();
        this.obstaclePlacement = ko.observable();

        // this.obstacles.push(new Objects.Obstacle(Objects.ObstacleType.Camera, 145, Objects.ObstaclePlacement.After))
        // this.obstacles.push(new Objects.Obstacle(Objects.ObstacleType.Camera, 4, Objects.ObstaclePlacement.After))
        // this.obstacles.push(new Objects.Obstacle(Objects.ObstacleType.Camera, 177, Objects.ObstaclePlacement.After))
        // this.obstacles.push(new Objects.Obstacle(Objects.ObstacleType.Camera, 77, Objects.ObstaclePlacement.After))
    }

    public addObstacle = () => {
        if (!this.obstacleIndex() || !this.obstacleType()) return;

        const type = utils.mapToNumber(this.obstacleType()) as Objects.ObstacleType;
        const seatNumber = utils.mapToNumber(this.obstacleIndex());
        const placement = utils.mapToNumber(this.obstaclePlacement()) as Objects.ObstaclePlacement; 

        this.obstacles.push(new Objects.Obstacle(type , seatNumber, placement));
    };

    public removeObstacle = (obstacle: Objects.Obstacle) => {
        this.obstacles.remove(obstacle);
    }
}

class RowsViewModel {
    rowControls: KnockoutObservableArray<Objects.Control>;
    seatsAmount: KnockoutComputed<number>;

    constructor() {
        this.rowControls = ko.observableArray([
            {
                label: 'Lower right corner behind goal',
                value:  ko.observable(30)
            },
            {
                label: 'Upper right corner behind goal',
                value:  ko.observable(30)
            },
            {
                label: 'Far touchrow right',
                value:  ko.observable(30)
            },
            {
                label: 'Far touchrow left',
                value:  ko.observable(30)
            },
            {
                label: 'Upper left corner behind goal',
                value:  ko.observable(30)
            },
            {
                label: 'Lower left corner behind goal',
                value:  ko.observable(3)
            }
        ]);
        
        this.seatsAmount = ko.pureComputed(() => this.rowControls().map((rowControl) => {
            let value: any = rowControl.value();
            return parseInt(value)
        }).reduce((prev, curr) => prev + curr))
    }
}

export default class PhotoPitchViewModel {
    builder: PhotoPitchBuilder;
    selectedSeat: KnockoutObservable<any>;
    leftHandTeam: KnockoutObservable<string>;
    rightHandTeam: KnockoutObservable<string>;
    clockwise: KnockoutObservable<boolean>;
    rowControls: KnockoutObservableArray<Objects.Control>;
    seatsAmount: KnockoutComputed<number>;
    obstacles: KnockoutObservableArray<Objects.Obstacle>;
    unavailableSeats: KnockoutComputed<number[]>;
    entranceControl: KnockoutObservable<Objects.EntrancePlacement>;
    isReady: KnockoutObservable<boolean>;
    view: PhotoPitchView;
    
    constructor() {
        this.builder = new PhotoPitchBuilder();
        this.selectedSeat = ko.observable(null);
        this.leftHandTeam = ko.observable('Team 1');
        this.rightHandTeam = ko.observable('Team 2');
        this.clockwise = ko.observable(true);
        this.entranceControl = ko.observable(Objects.EntrancePlacement.Center);
        this.isReady = ko.observable(true);
        const unavailableSeatsViewModel = new UnavailableSeatsViewModel();
        const obstaclesViewModel = new ObstacleViewModel();
        const rowsViewModel = new RowsViewModel();

        ko.utils.extend(this, unavailableSeatsViewModel);
        ko.utils.extend(this, obstaclesViewModel);
        ko.utils.extend(this, rowsViewModel);

        this.builder.build(this);

        ko.utils.extend(this, new PhotoPitchView(this));
    }

    public selectSeat = (seat: Objects.Seat) => {
        if (this.selectedSeat()) this.selectedSeat().state(Objects.SeatState.Available);

        this.selectedSeat(seat);
        seat.state(Objects.SeatState.AvailableSelected);
    };

    public unselectSeat = () => {
        if (this.selectedSeat()) this.selectedSeat().state(Objects.SeatState.Available);

        this.selectedSeat(null);
    };
}


const photoPitchViewModel = new PhotoPitchViewModel();

ko.applyBindings(photoPitchViewModel);
