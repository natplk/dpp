
import * as ko from "knockout";
import * as utils from "./utils/utils";
import * as CONST from "./utils/const";
import * as Objects from "./objects";
import * as View from "./view";
import PhotoPitchViewModel from "./photoPitch";

export class ViewConfig {
        width: number;
        height: number;
        unit: number;
        margin: View.Offset;
        marginOuter: View.Offset;
        marginInner: View.Offset;
    }

export class ViewElement {
    index: number;
    parentIndex: number;
    item: Objects.IDrawable;
}

export class PhotoPitchView {
    viewModel: PhotoPitchViewModel;
    viewConfig: KnockoutComputed<ViewConfig>;
    viewStack: KnockoutObservableArray<ViewElement>;
    view: View.View;
    rows: KnockoutObservableArray<Objects.Row>;
    entrance: KnockoutObservable<Objects.Entrance>;
    indicator: KnockoutObservable<Objects.Indicator>;
    indicatorPreview: KnockoutObservable<Objects.Indicator>;
    names: KnockoutComputed<View.CaptionWrapped[]>;

    constructor(viewModel: PhotoPitchViewModel) {
        this.viewModel = viewModel;
        this.viewStack = ko.observableArray([]);
        this.rows = this.viewModel.builder.rows;
        this.entrance = this.viewModel.builder.entrance;
        this.indicator = this.viewModel.builder.indicator;
        this.indicatorPreview = ko.observable(null);

        this.init();
    }

    init() {
        let width = document.body.getBoundingClientRect().width;
        let height = CONST.ratio * width;

        this.viewConfig = ko.pureComputed(() => this.calculateViewConfig(width, height)).extend({ throttle: 300 });
        this.view = new View.View(this.viewConfig());
        this.names = ko.computed(() => this.drawNames(this.viewModel.leftHandTeam(), this.viewModel.rightHandTeam()));

        this.drawEntrance();
        this.drawRows();

        this.viewModel.isReady(true);

        this.indicator.subscribe(this.drawIndicator);
        this.indicatorPreview.subscribe(this.drawIndicatorPreview);
        this.entrance.subscribe(this.drawEntrance);
        this.viewStack.subscribe(this.onViewStackChange);

        this.viewConfig.subscribe((viewConfig) => {
            this.view.reinit(viewConfig);

            this.drawRows();
            this.drawEntrance();
            this.mutateSelectedSeat();
           
            this.viewStack.removeAll();
            this.viewModel.isReady(true);
        });
    }

    drawNames(name1: string, name2: string) {
        let background = this.view.background;
        let maxWidth = background.width() / 2;
        let size = maxWidth / 30;
        let x = background.x() + maxWidth / 2;
        let x2 = x + maxWidth;
        let y = background.y() + background.height() - size;

        return [
            new View.CaptionWrapped(name1, x, y, size, maxWidth),
            new View.CaptionWrapped(name2, x2, y, size, maxWidth)
        ];
    }

    drawIndicator = () => {
        let indicator = this.indicator();

        if (!indicator) {
            this.indicatorPreview(null);
            return;
        }

        const { width, margin } = this.viewConfig();
        let size = new View.Size(0.1 * margin.x);
        let x = indicator.referenceBox.x + (indicator.referenceBox.width - size.width) / 2;
        let y = indicator.referenceBox.y + (indicator.referenceBox.height - size.height) / 2;

        indicator.size = size;
        
        if (indicator.referenceBox.y > margin.y) {
            if (indicator.referenceBox.x > width / 2) {
                indicator.placement = Objects.IndicatorPlacement.Right;
            } else {
                indicator.placement = Objects.IndicatorPlacement.Left;
            }
        }

        indicator.draw(x, y);
        this.indicatorPreview(new Objects.Indicator(indicator.referenceBox)); 
    }

    drawIndicatorPreview = () => {
        let indicatorPreview = this.indicatorPreview();

        if (!indicatorPreview) return;

        const { width, margin } = this.viewConfig();
        let size = new View.Size(0.8 * margin.x);
        let x = indicatorPreview.referenceBox.x + (indicatorPreview.referenceBox.width - size.width) / 2;
        let y = indicatorPreview.referenceBox.y + (indicatorPreview.referenceBox.height - size.height) / 2;

        indicatorPreview.size = size;
        indicatorPreview.placement = this.indicator().placement;

        indicatorPreview.draw(x, y);
    }

    drawEntrance = () => {
        const { margin } = this.viewConfig();

        this.entrance().size = new View.Size(margin.y / 3);
        this.entrance().draw(0, 0, this.viewConfig());
    }

    drawRows = () => {
        let viewConfig = this.viewConfig();
        let width = viewConfig.width;
        let height = viewConfig.height;
        let unit = viewConfig.unit;
        let margin = viewConfig.margin;
        let marginInner = viewConfig.marginInner;
        let marginOuter = viewConfig.marginOuter;
        let seatSize = new View.Size(unit);
        let obstacleSize = new View.Size(CONST.unitsInObstacle * unit);
        let space = CONST.unitsInSpaceBeetweenSeats * unit;

        this.rows().map((row) => {
            row.groups
                .map((group) => {
                    group.seats.map(seat => seat.size = seatSize);
                    group.obstacles.map(obstacle => obstacle.size = obstacleSize);
                });
        });

        this.rows()
            .map((row, index) => {
                let x = 0;
                let y = 0;
                let offsetStart = {x: 0, y: 0};
                let size: View.Size;

                if (row instanceof Objects.RightLeftRow || row instanceof Objects.LeftRightRow) {
                    size = new View.Size(width / 2 - margin.x, margin.y);
                } else {
                    size = new View.Size(margin.y, height / 2 - margin.y);
                }

                switch (index) {
                    case 0:
                        x = width - margin.x;
                        y = height - margin.y;
                        offsetStart.x = marginInner.y - seatSize.width - space;
                    break;
                    case 1:
                        x = width - margin.x;
                        y = margin.y;
                        offsetStart.x = marginInner.y - seatSize.width - space;
                    break;
                    case 2:
                        x = width - margin.x;
                        offsetStart.y = marginOuter.y;
                    break;
                    case 3:
                        x = margin.x;
                        offsetStart.y = marginOuter.y;
                    break;
                    case 4:
                        x = margin.x - margin.y;
                        y = margin.y;
                        offsetStart.x = margin.y - marginInner.y;
                    break;
                    case 5:
                        x = margin.x - margin.y;
                        y = height - margin.y;
                        offsetStart.x = margin.y - marginInner.y;
                    break;
                };

                row.size = size;
                row.offsetStart = offsetStart;
                row.draw(x, y, viewConfig);
            });
    }

    mutateSelectedSeat = () => {
        let selectedSeat = this.viewModel.selectedSeat();

        if (!selectedSeat) return;

        this.rows().map(row => row.groups.map(group => group.seats.map(seat => {
            if (seat.number === selectedSeat.number) {
                this.viewModel.selectedSeat(seat)
                this.viewModel.selectedSeat.valueHasMutated();
            } 
        })))
    }

    private getViewStackLast = (viewStack: ViewElement[]) => {
        let len = viewStack.length;

        return len ? viewStack[len - 1] : null
    }

    private onViewStackChange = (viewStack: ViewElement[]) => {
        let viewStackLast = this.getViewStackLast(viewStack);
       
        if (viewStackLast) {
            let item = viewStackLast.item;
            let shape = (item.shape instanceof View.Box) ? <View.Box> item.shape : item.boundingBox;

            this.changeVisibility(viewStackLast);
            this.view.changeViewBox(shape.x, shape.y, shape.width, shape.height);
        } else {
            this.view.setOriginViewBox();
            this.changeVisibility(null);
        }
    }

    private changeVisibility(viewElement: ViewElement) {
        let indicator = this.indicator();
        let rows = this.rows();

        if (!viewElement) {
            rows.map((row: Objects.Row) => {
                row.isVisible(true);
                row.groups.map(group => group.isVisible(true));
            });
            if (indicator && indicator.isVisible()) indicator.isVisible(true);
            return;
        }

        let {index, parentIndex, item} = viewElement;

        if (item instanceof Objects.Row) {
            rows.map((row: Objects.Row, i: number) => {
                let isRowVisible = i === index;
            
                row.isVisible(isRowVisible);
                row.groups.map(group => {
                    group.isVisible(isRowVisible);
                    if (group.hasSelectedSeat()) indicator.isVisible(isRowVisible);
                });
            });
        } else if (item instanceof Objects.Group) {
            rows.map((row: Objects.Row, i: number) => {
                let isRowVisible = i === parentIndex;
                
                row.isVisible(isRowVisible);
                row.groups.map((group: Objects.Group, i: number) => {
                    let isGroupVisible = isRowVisible && (i === index);
                    
                    group.isVisible(isGroupVisible);
                    if (group.hasSelectedSeat()) indicator.isVisible(isGroupVisible); 
                });
            });
        }
    }

    private calculateViewConfig = (width: number, height: number) => {
        const unitsInMarginOuter = CONST.unitsInMarginOuter;
        const unitsInMarginInner = CONST.unitsInMarginInner;
        const unitsInMargin = unitsInMarginOuter + unitsInMarginInner;
        const unitsInObstacle = CONST.unitsInObstacle;
        const ratio = height / width;

        let calculateUnitSize = (units: number, bias: number = 0) => {
            return Math.min(...this.rows().map((row: Objects.Row) => {
                const seatsAmount = row.getSeatsAmount();
                const obstaclesAmount = row.getObstaclesAmount() * unitsInObstacle;
                let rowSize: number;
                let rowUnitsInMargin = units;
                let rowBias = bias;

                if (row instanceof Objects.RightLeftRow || row instanceof Objects.LeftRightRow) {
                    rowSize = width;
                } else {
                    rowSize = height;
                    rowUnitsInMargin *= ratio;
                    rowBias *= ratio;
                }

                return (rowSize / 2 - rowBias) / (rowUnitsInMargin + (seatsAmount + obstaclesAmount) * (1 + CONST.unitsInSpaceBeetweenSeats))
            }));
        };

        let unitSize = calculateUnitSize(unitsInMargin);
        let marginOuterSize = unitsInMarginOuter * unitSize;
        let marginInnerSize = unitsInMarginInner * unitSize;
        let marginSize = unitsInMargin * unitSize;
        let min = 0.1 * height;
        let max = 0.2 * height;

        if (marginSize < min) {
            marginSize = min;
            marginInnerSize = marginSize * unitsInMarginInner / unitsInMargin;
            marginOuterSize = marginSize * unitsInMarginOuter / unitsInMargin;
            unitSize = calculateUnitSize(0, marginSize);
        } else if (marginSize > max) {
            marginSize = max;
            unitSize = marginSize / unitsInMargin;
            marginInnerSize = unitsInMarginInner * unitSize;
            marginOuterSize = unitsInMarginOuter * unitSize;
        }

        let unit = utils.round(unitSize);
        let marginOuter = new View.Offset(marginOuterSize, marginOuterSize, ratio);
        let marginInner = new View.Offset(marginInnerSize, marginInnerSize, ratio);
        let margin = new View.Offset(marginSize, marginSize, ratio);

        return {
            width: width,
            height: height,
            unit: unit,
            margin: margin,
            marginOuter: marginOuter,
            marginInner: marginInner
        }
    }

    public changeViewRequest = (index: number, parentIndex: number, item: Objects.IDrawable) => this.viewStack.push({ index,parentIndex, item});

    public backToPreviousView = () => this.viewStack.pop();

    public resetView = () => this.viewStack.removeAll();

    public isDefaultStep = ko.pureComputed(() => this.viewStack().length === 0);

    public isFirstStep = ko.pureComputed(() => {
        let viewStackLast = this.getViewStackLast(this.viewStack());

        return viewStackLast ? viewStackLast.item instanceof Objects.Row : false;
    })

    public isSecondStep = ko.pureComputed(() => {
        let viewStackLast = this.getViewStackLast(this.viewStack());

        return viewStackLast ? viewStackLast.item instanceof Objects.Group : false;
    });
}