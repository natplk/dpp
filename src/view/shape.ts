export interface IShape {
    x: number;
    y: number;
    strokeWidth: number;
    transform: string;
    rotate(degree: number, x?: number, y?: number): void
    translate(x: number, y: number): void
}