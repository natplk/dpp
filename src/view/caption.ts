import * as utils from "../utils/utils";
import { IShape } from "./shape";

export class CaptionSpan {
    value: string;
    x: number;
    dy: number; 

    constructor(value: string, x: number, dy: number) {
        this.value = value;
        this.x = x;
        this.dy = dy;
    }
}

export class Caption implements IShape {
    value: string;
	x: number;
	y: number;
	size: number;
    strokeWidth: number = 0;
    transform: string = "";
  
    constructor(value: string, x: number, y: number, size: number) {
        this.value = value;
        this.x = utils.round(x);
        this.y = utils.round(y);
        this.size = utils.round(size);
    }

    rotate(degree: number) {
        this.transform = `translate(${this.x}, ${this.y}) rotate(${degree}) translate(${-this.x}, ${-this.y})`;
    }

    translate(x: number, y: number) {
        this.transform += ` translate(${x}, ${y}) `;
    }
}

export class CaptionWrapped extends Caption {
    tspans: CaptionSpan[];
    maxWidth: number;

    constructor(value: string, x: number, y: number, size: number, maxWidth: number) {
        super(value, x, y, size);
        this.maxWidth = maxWidth;
        this.tspans = [];
        this.wrapText()
    }

    addTspan(value: string, x: number = 0, dy: number = 0) {
        this.tspans.push({value, x, dy});
        this.y -= dy;
    }
 
    wrapText() {
        const len = this.value.length;
        const words = this.value.split(' ');
        const onlyOneWord = words.length === 1;
        const predictableWidth = len * this.size;
        const max = utils.round(this.maxWidth / this.size);

        if ((predictableWidth > this.maxWidth) && onlyOneWord) {
            this.addTspan(this.value.slice(1,max) + '...', this.x);
            return
        } else if (predictableWidth <= this.maxWidth) {
            this.addTspan(this.value, this.x)
            return
        }

        let line = '';
        let dy = 0;

        for (let i = 0; i < words.length; i++) {
            let word = words[i] + ' ';
            let tmpLine = line + word;

            if (tmpLine.length > max) {

                if (line === '') {
                    let chunks = utils.splitWord(word, max);
                    let len = chunks.length;
                    let rest = chunks[len - 1];

                    for (let i= 0; i < len - 1; i++) {
                        this.addTspan(chunks[i], this.x, dy);
                    }

                    line = rest + ' ';
                } else {
                    this.addTspan(line, this.x, dy);
                    line = word;
                }
                dy = this.size;
            }
            else {
                line = tmpLine;
            }
        }

        this.addTspan(line, this.x, dy)
    }
}