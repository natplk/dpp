import * as ko from "knockout";
import * as utils from "../utils/utils";
import { IShape } from "./shape";
import { Line } from "./line";

export class Box implements IShape {
    x: number;
    y: number;
    width: number;
    height: number;
    strokeWidth: number = 0;
    transform: string = "";
    borders: Line[];

    constructor(x: number, y: number, width: number, height: number) {
        this.x = utils.round(x);
        this.y = utils.round(y);
        this.width = utils.round(width);
        this.height = utils.round(height);
    }

    rotate(degree: number) {
        let x = this.x + this.width / 2;
        let y = this.y + this.height / 2;
        this.transform = `translate(${x}, ${y}) rotate(${degree}) translate(${-x}, ${-y}) `;
    }

    translate(x: number, y: number) {
        this.transform += ` translate(${x}, ${y}) `;
    }

    getCordsOfBox(bias: number = 0) {
        return {
            x1: this.x + bias,
            y1: this.y + bias,
            x2: this.x + this.width + bias,
            y2: this.y + this.height + bias
        }
    }

    setBorders(strokeWidth: number, placement?: string) {
        const bias = strokeWidth / 2;
        const {x1, y1, x2, y2} = this.getCordsOfBox(bias);

        let top: Line = {x1, y1, x2, y2: y1, strokeWidth};
        let right: Line = {x1: x2, y1, x2, y2, strokeWidth};
        let bottom: Line = {x1, y1: y2, x2, y2, strokeWidth};
        let left: Line = {x1, y1, x2: x1, y2, strokeWidth};

        if (placement === 'vertical') {
            this.borders = [right, left];
        } else if (placement === 'horizontal') {
            this.borders = [top, bottom];
        } else {
            this.borders = [top, right, left, bottom];
        }
    }
}
