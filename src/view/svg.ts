import * as ko from "knockout";
import * as utils from "../utils/utils";
import * as CONST from "../utils/const";

export class SvgViewBox {
    x: number;
    y: number;
    width: number;
    height: number;
}

export class Svg {
    x: KnockoutObservable<number>;
    y: KnockoutObservable<number>;
    width: KnockoutObservable<number>;
    height: KnockoutObservable<number>;
    ratio: KnockoutComputed<number>;
    viewBoxString: KnockoutComputed<string>;
	private _viewBox: KnockoutObservable<SvgViewBox>;
	private _originViewBox: KnockoutObservable<SvgViewBox>;

    constructor(width: number, height: number, x: number = 0, y: number = x) {
        this.x = ko.observable(utils.round(x));
        this.y = ko.observable(utils.round(y));
        this.width = ko.observable(utils.round(width));
        this.height = ko.observable(utils.round(height));
        this.ratio = ko.computed(() => this.height() / this.width());

        let initialViewBox = {x: 0, y: 0, width: this.width(), height: this.height()};

        this._originViewBox = ko.observable({...initialViewBox});
        this._viewBox = ko.observable({...initialViewBox});

        this.viewBoxString = ko.computed(() => this.getViewBoxString(this._viewBox()))
    }

    set originViewBox(viewBox: SvgViewBox) {
        this._originViewBox(viewBox);
    }

    get originViewBox(): SvgViewBox {
        return this._originViewBox();
    }

    set viewBox(viewBox: SvgViewBox) {
        this._viewBox(viewBox);
    }

    get viewBox(): SvgViewBox {
        return this._viewBox();
    }

    public initViewBox(initialViewBox: SvgViewBox) {
        this._originViewBox({...initialViewBox});
        this._viewBox({...initialViewBox});
    }

    public setOriginViewBox() {
        this._viewBox({...this._originViewBox()});
    }

    public getViewBoxString = (viewBox: SvgViewBox) => {
        return `${viewBox.x}, ${viewBox.y}, ${viewBox.width}, ${viewBox.height}`;
    };
}

