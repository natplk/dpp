import { IShape } from "./shape";

export class Polygon implements IShape {
    x: number;
    y: number;
    points: string;
    strokeWidth: number = 0;
    transform: string = "";

    constructor(points: string) {
        this.points = points;
        this.transform = '';
    }

    rotate(degree: number, x: number, y: number) {
        this.transform += `translate(${x}, ${y}) rotate(${degree}) translate(${-x}, ${-y}) `;
    }

    translate(x: number, y: number) {
        this.transform += `translate(${x}, ${y}) `;
    }
}
