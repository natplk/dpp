
import * as utils from "../utils/utils";
import * as CONST from "../utils/const";

import { SvgViewBox, Svg } from "./svg";
import { Background } from "./background";
import { Offset } from "./offset";

import { ViewConfig } from "../photoPitchView"

export class View extends Svg {
    background: Background;
    minimum: number;
    preview: Svg;

    constructor(viewConfig: ViewConfig) {
        super(viewConfig.width, viewConfig.height);

        this.initViewBox({
            x: 0, 
            y: (- viewConfig.marginOuter.y / 2), 
            width: this.width(), 
            height: this.height()}
        );

        this.background = new Background(viewConfig, CONST.stripes);
        this.minimum = viewConfig.unit * CONST.seatsInGroup;
        this.drawPreview(viewConfig);
    }

reinit(viewConfig: ViewConfig) {
    this.background.reinit(viewConfig);
    this.drawPreview(viewConfig);
}

drawPreview(viewConfig: ViewConfig) {
    this.preview = new Svg(this.width(), this.height());
}

changeViewBox = (x: number, y: number, width: number, height: number) => {
    const viewMargin = this.background.y();
    const isHorizontal = y < viewMargin || y >= this.height();
    const isRight = x >= (this.width() / 2);
    const min = this.minimum;
    const marginRatio = 0.2;
    const ratio = this.ratio();

    let offset: Offset = { x: 0, y: 0 };
    let margin: Offset = { x: 0, y: 0 };
    let viewBox: SvgViewBox = { x: 0, y: 0, width: 0, height: 0 };
    let carrier: number;

    if (isHorizontal) {
        carrier = width;

        if (carrier <= min) {
            carrier = min;
            offset.x = (min - width) / 2;
        }

        margin.x = marginRatio * carrier;
        margin.y = ratio * viewMargin;
        viewBox.x = x - margin.x - offset.x;
        viewBox.y = y - margin.y - offset.y;
        viewBox.width = carrier + 2 * margin.x;
        viewBox.height = ratio * viewBox.width;

        if (isRight) {
            viewBox.x = x - margin.x - offset.x;
        }
    } else {
        carrier = height;

        if (carrier <= min) {
            carrier = min;
            offset.y = (min - height) / 2;
        }

        margin.y = marginRatio * carrier;
        margin.x = ratio * viewMargin;
        viewBox.x = x - margin.x - offset.x;
        viewBox.y = y - margin.y - offset.y;
        viewBox.height = carrier + 2 * margin.y;
        viewBox.width = ratio * viewBox.height;

        if (isRight) {
            viewBox.x = x + margin.x - offset.x + width - viewBox.width;
        }
    }

    this.viewBox = {
        x: utils.round(viewBox.x),
        y: utils.round(viewBox.y),
        width: utils.round(viewBox.width),
        height: utils.round(viewBox.height)
    };
}
}
