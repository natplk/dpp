import * as utils from "../utils/utils";

export class Offset {
    x: number;
    y: number;

    constructor(x: number, y: number = x, ratio: number = 1) {
        this.x = utils.round(x);
        this.y = utils.round(y);
        
        if (ratio) {
            this.y = utils.round(x * ratio)
        }
    }
}