import * as utils from "../utils/utils";
import * as CONST from "../utils/const";

import { SvgViewBox, Svg } from "./svg";
import { ViewConfig } from "../photoPitchView"

export class Background extends Svg {
        stripes: number;
        stripeWidth: number;
        stripeHeight: number;

        constructor(viewConfig: ViewConfig, stripes: number) {
            const {width, height, margin} = viewConfig;
            const x = margin.x;
            const y = margin.y;

            super(width - 2 * x, height - 2 * y, x, y);

            this.stripes = stripes;
            this.initViewBox({ x: 0, y: 0, width: 1000, height: 640 });
            this.stripeWidth = utils.round(this.viewBox.width / this.stripes);
            this.stripeHeight = this.viewBox.height;
        }

        reinit(viewConfig: ViewConfig) {
            const {width, height, margin} = viewConfig;
            const x = margin.x;
            const y = margin.y;

            this.x(x);
            this.y(y);
            this.width(width - 2 * x);
            this.height(height - 2 * y);
        }
    }