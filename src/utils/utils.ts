
export function round(number: number): number {
    return Math.round(number * 10) / 10;
}

export function mapToNumber(value: any): number {
    if (typeof value === "string") {
        return parseFloat(value)
    } else if ( typeof value === "number") {
        return value
    }

    return 0
}

export function arraysEqual(a: number[], b : number[]): boolean {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    for (let i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }

    return true;
}

// export function arraysContain<T>(a: KnockoutObservableArray<T>, b: T[]) {
//     a.push.apply(a, b);
// }

export function randomWithProbability() {
    let idx = Math.floor(Math.random() * 10);
    return idx % 5 === 0;
}

export function compareKeys(a: object, b: object) {
  var aKeys = Object.keys(a).sort();
  var bKeys = Object.keys(b).sort();
  return JSON.stringify(aKeys) === JSON.stringify(bKeys);
}

export function getDelta(a: any, b: any) {
    if (!compareKeys(a, b)) return
    let delta: any = {};

    for(let key in a) {
        if (a.hasOwnProperty(key)) {
            delta[key] = a[key] - b[key];
        }
    }

    return delta;
}

export function compareObjects(a: object, b: object): boolean {
    return JSON.stringify(a) === JSON.stringify(b);
}

export function splitWord(word: string, maxChars: number) {
    let chunks: string[] = [];

    function slice(word: string): any {
        const len = word.length;

        if (len > maxChars) {
            const chunk = word.slice(1, maxChars) + '-';
            const rest = word.slice(maxChars + 1, len - 1);

            chunks.push(chunk);

            slice(rest);
        } else {
            chunks.push(word);
        }
    }
    
    slice(word);

    return chunks;
}

// setGroundPath() {
//    const bias = 2 * this.ground.stroke;
//    const cords = this.getCords(this.ground);
//    const cords2 = this.getCords(this.ground, bias);

//     this.groundPath = `M${cords2.x2},${cords2.y1}V${cords2.y2}H${cords2.x1}V${cords2.y1}H${cords2.x2} 
//         M${cords.x2},${cords.y1}H${cords.x1}V${cords.y2}H${cords.x2}V${cords.y1}L${cords.x2},${cords.y1}z`;
// }

export function isPrime(number: number):boolean {
    let i = 2;
    let r = Math.sqrt(number);

    if (number === 1) return false;

    while (i <= r) {
        if (!(number % i++)) return false
    };

    return true
}


export function factorial(number: number):number {
    let i = number;
    let result = 1;

    while (i > 1) {
        result *= i;
        i--;
    }

    return result;
}

function fibonacci(number: number): number { // O(n)
  let a = 1;
  let b = 0; 
  let temp;

  while (number >= 0) {
    temp = a;
    a = a + b;
    b = temp;
    number--;
  }

  return b;
}

function fibonacci2(number: number): number { // O(n)
    if (number <= 1) return 1;

    return fibonacci2(number - 1) + fibonacci2(number - 2)
}

export function isSorted(arr: any[]): boolean {
    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i] > arr[i + 1]) return false
    }

    return true
}

export function filter<T>(arr: T[], func: Function): T[] {
    let result: T[] = [];

    for (let i = 0; i < arr.length; i++) {
        if (func(arr[i])) result.push(arr[i]);
    }

    return result
}

export function reduce<T>(arr: T[], func: Function, initial: any): any {
    let prevValue = initial;

    for (let i = 0; i < arr.length; i++) {
        prevValue = func(prevValue, arr[i])
    }

    return prevValue
}

export function reverse<T>(text: string): string {
    let result = [];
    let len = text.length;

    for (let i = 0; i < len; i++) {
        result[i] = text[len - 1 - i];
    }

    return result.toString();
}