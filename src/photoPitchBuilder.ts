import * as ko from "knockout";
import * as utils from "./utils/utils";
import * as CONST from "./utils/const";
import * as Objects from "./objects";
import PhotoPitchViewModel from "./photoPitch";

export class PhotoPitchBuilder {
        private _viewModel: PhotoPitchViewModel;
        private _indicator: KnockoutObservable<Objects.Indicator>;
        private _entrance: KnockoutObservable<Objects.Entrance>;
        private _rows: KnockoutObservableArray<Objects.Row>;

        constructor() {
            this._entrance = ko.observable(null);
            this._indicator = ko.observable(null);
            this._rows = ko.observableArray([]);
        }

        get rows(): KnockoutObservableArray<Objects.Row> {
            return this._rows;
        }

        get entrance(): KnockoutObservable<Objects.Entrance> {
            return this._entrance;
        }

        get indicator(): KnockoutObservable<Objects.Indicator> {
            return this._indicator;
        }

        build = (viewModel: PhotoPitchViewModel) => {
            this._viewModel = viewModel;

            this.buildEntrance(viewModel.entranceControl());

            viewModel.entranceControl.subscribe(this.buildEntrance);
            viewModel.selectedSeat.subscribe(this.buildIndicator);

            ko.computed(() => {
                let rowControls = viewModel.rowControls();
                let obstacles = viewModel.obstacles();
                let unavailableSeats = viewModel.unavailableSeats();
                let clockwise = viewModel.clockwise();

                viewModel.isReady(false);

                this._rows(this.buildRows(rowControls));
            });
        }

        buildRows(rowControls: Objects.Control[]) {
            let clockwise = this._viewModel.clockwise();
            let initialValue = (clockwise) ? this._viewModel.seatsAmount() : 1;
            let prevValue = initialValue;
            let sumValue = initialValue;
            let rows: Objects.Row[] = [];

            rowControls.map((rowControl: Objects.Control, index: number) => {
                let row: Objects.Row;
                let label = rowControl.label;
                let order: Objects.Order;
                let numberStart: number;
                let rowSeatsAmount = rowControl.value();

                if (clockwise) {
                    sumValue -= rowSeatsAmount;
                } else {
                    sumValue += rowSeatsAmount;
                }

                switch (index) {
                case 0:
                    row = new Objects.BottomTopRow(label);
                    break;
                case 1:
                    row = new Objects.TopBottomRow(label);
                    break;
                case 2:
                    row = new Objects.RightLeftRow(label);
                    break;
                case 3:
                    row = new Objects.LeftRightRow(label);
                    break;
                case 4:
                    row = new Objects.TopBottomRow(label);
                    break;
                case 5:
                    row = new Objects.BottomTopRow(label);
                    break;
                default:
                    row = new Objects.BottomTopRow(label);
                };

                row.setGroups(this.buildGroups(row, rowSeatsAmount));

                if ((index === 0 && row instanceof Objects.BottomTopRow) ||
                    (index === 4 && row instanceof Objects.TopBottomRow) ||
                    row instanceof Objects.RightLeftRow) {
                    numberStart = prevValue;
                    if (clockwise) {
                        order = Objects.Order.Descending;
                    } else {
                        order = Objects.Order.Asceding;
                    }
                } else {
                    if (clockwise) {
                        order = Objects.Order.Asceding;
                        numberStart = prevValue - rowSeatsAmount + 1;
                    } else {
                        order = Objects.Order.Descending;
                        numberStart = prevValue + rowSeatsAmount - 1;
                    }
                }

                row.order = order;
                row.setNumbers(numberStart);

                this.buildObstacles(row);
                this.seatsDecorate(row);

                rows.push(row);

                prevValue = sumValue;
            });

            return rows;
        }

        buildGroups(row: Objects.Row, seatsAmount: number) {
            let groups: Objects.Group[] = [];
            let groupAmount = CONST.seatsInGroup;
            let remainingGroupAmount = seatsAmount % CONST.seatsInGroup;
            let groupsNumber = seatsAmount / CONST.seatsInGroup;

            if (remainingGroupAmount) {
                groupsNumber = Math.floor(groupsNumber) + 1;
            }

            for (let i = 0; i < groupsNumber; i++) {
                let isLastRemaining = ((i + 1) === groupsNumber) && remainingGroupAmount !== 0;
                let groupSeatsAmount = isLastRemaining ? remainingGroupAmount : groupAmount;
                let label = i.toString();
                let group: Objects.Group;

                if (row instanceof Objects.BottomTopRow) {
                    group = new Objects.BottomTopGroup(label);
                } else if (row instanceof Objects.RightLeftRow) {
                    group = new Objects.RightLeftGroup(label);
                } else if (row instanceof Objects.LeftRightRow) {
                    group = new Objects.LeftRightGroup(label);
                } else if (row instanceof Objects.TopBottomRow) {
                    group = new Objects.TopBottomGroup(label);
                } else {
                    group = new Objects.BottomTopGroup(label);
                }

                group.setSeats(this.buildSeats(groupSeatsAmount));

                groups.push(group);
            }

            return groups;
        }

        buildSeats(groupSeatsAmount: number) {
            let seats: Objects.Seat[] = [];

            for (let i = 0; i < groupSeatsAmount; i++) {
                seats.push(new Objects.Seat());
            }

            return seats;
        }

        buildObstacles(row: Objects.Row) {
            row.groups.map(group => {
                let obstacles: Objects.Obstacle[] = [];

                group.seats.map(seat => {
                    let seatObstacles = this._viewModel.obstacles()
                        .filter((o: Objects.Obstacle) => o.seatNumber === seat.number);

                    if (group.order === Objects.Order.Descending) {
                        seatObstacles.map((o: Objects.Obstacle) => {
                            let placement = o.placement;
                            if (placement === Objects.ObstaclePlacement.After) o.placement = Objects.ObstaclePlacement.Before;
                            if (placement === Objects.ObstaclePlacement.Before) o.placement = Objects.ObstaclePlacement.After;
                        });
                    }

                    obstacles.push.apply(obstacles, seatObstacles);
                });

                group.obstacles = obstacles;
            });
        }

        seatsDecorate(row: Objects.Row) {
            row.groups.map(group => {
                group.seats.map(seat => {
                    this.setSeatState(seat);
                    this.setSeatLabel(seat, `${row.label}, <br>Section: ${group.label}`)
                });
            });
        }

        setSeatState(seat: Objects.Seat) {
            let isTaken = !!this._viewModel.unavailableSeats()
                .filter((seatNumber: number) => seatNumber === seat.number)
                .length;

            if (isTaken) seat.state(Objects.SeatState.Taken);    
        }

        setSeatLabel(seat: Objects.Seat, label: string) {
            seat.label = label;
        }

        buildEntrance = (placement: Objects.EntrancePlacement) => {
            this._entrance(new Objects.Entrance(placement));
        }

        buildIndicator = (seat: Objects.Seat) => {
            if (!seat) {
                this.indicator(null);
                return;
            };

            this._indicator(new Objects.Indicator(seat.boundingBox));
        }
    }