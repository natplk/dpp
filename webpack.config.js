const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    entry: {
        photoPitch: [
            './src/photoPitch.ts'
        ]
    },
    output: {
        filename: './built/[name].js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract([
                    'css-loader',
                    'postcss-loader'
                ]),
            },
            { test: /\.ts$/, exclude: /node_modules/, loader: 'ts-loader' },
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    plugins: [new ExtractTextPlugin('./built/photo-pitch.css')]
};